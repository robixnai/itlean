O projeto tem como objetivo avaliar os fundamentos que um
desenvolvedor Android precisa dominar para um bom desempenho na
esfera técnica. O fluxo da aplicação é livre, mas precisa atender ao
Guideline da Google e as melhores práticas, o usuário deve poder
navegar entre as notícias, ver o detalhe da notícia, compartilhar com
um app terceiro, salvar notícia para ler mais tarde, deletar notícia na
lista de notícias salvas.
Link para o RSS.
http://www.androidauthority.com/feed/
Itens avaliados
- Usabilidade;
- Uso de Autolayout;
- Uso de Libs externas (Gradle);
- design pattern;
- Controle de versão;
- Gerenciamento de Cache;
- ORM;
