package br.com.rmoreira.app;

import android.app.Application;
import android.content.Context;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.Configuration;

import java.io.File;

import br.com.rmoreira.app.dao.entities.NewsEntity;
import br.com.rmoreira.app.helpers.InternetHelper;

/**
 * Created by robsonmoreira on 10/10/17.
 */

public class AppRssApplication extends Application {

    private static AppRssApplication instance;

    @Override
    public void onCreate() {
        super.onCreate();

        instance = this;

        configureModels(this);
        ActiveAndroid.initialize(this);
    }

    public static boolean isNetworkAvailable() {
        return InternetHelper.isNetworkAvailable(instance.getApplicationContext());
    }

    public static File getFileCacheDir() {
        return instance.getCacheDir();
    }

    private void configureModels(final Context context) {
        Configuration.Builder configurationBuilder = new Configuration.Builder(context);
        configurationBuilder.addModelClasses(NewsEntity.class);
    }

}
