package br.com.rmoreira.app.dao.pojo;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;

/**
 * Created by robsonmoreira on 10/10/17.
 */

public class News implements Parcelable {

    private String title;
    private String link;
    private String comments;
    private Date pubDate;
    private String creator;
    private String description;
    private String content;
    private String imageUrl;
    private String commentRss;
    private boolean isSaved;

    public News() {
    }

    public News(String title, String link, String comments, Date pubDate, String creator, String description,
                String content, String imageUrl, String commentRss) {
        this.title = title;
        this.link = link;
        this.comments = comments;
        this.pubDate = pubDate;
        this.creator = creator;
        this.description = description;
        this.content = content;
        this.imageUrl = imageUrl;
        this.commentRss = commentRss;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public Date getPubDate() {
        return pubDate;
    }

    public void setPubDate(Date pubDate) {
        this.pubDate = pubDate;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getCommentRss() {
        return commentRss;
    }

    public void setCommentRss(String commentRss) {
        this.commentRss = commentRss;
    }

    public boolean isSaved() {
        return isSaved;
    }

    public void setSaved(boolean saved) {
        isSaved = saved;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.title);
        dest.writeString(this.link);
        dest.writeString(this.comments);
        dest.writeLong(this.pubDate != null ? this.pubDate.getTime() : -1);
        dest.writeString(this.creator);
        dest.writeString(this.description);
        dest.writeString(this.content);
        dest.writeString(this.imageUrl);
        dest.writeString(this.commentRss);
        dest.writeByte(this.isSaved ? (byte) 1 : (byte) 0);
    }

    protected News(Parcel in) {
        this.title = in.readString();
        this.link = in.readString();
        this.comments = in.readString();
        long tmpPubDate = in.readLong();
        this.pubDate = tmpPubDate == -1 ? null : new Date(tmpPubDate);
        this.creator = in.readString();
        this.description = in.readString();
        this.content = in.readString();
        this.imageUrl = in.readString();
        this.commentRss = in.readString();
        this.isSaved = in.readByte() != 0;
    }

    public static final Creator<News> CREATOR = new Creator<News>() {
        @Override
        public News createFromParcel(Parcel source) {
            return new News(source);
        }

        @Override
        public News[] newArray(int size) {
            return new News[size];
        }
    };

}
