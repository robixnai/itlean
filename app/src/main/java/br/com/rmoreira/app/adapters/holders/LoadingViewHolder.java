package br.com.rmoreira.app.adapters.holders;

import android.view.View;

/**
 * Created by robsonmoreira on 10/10/17.
 */

public class LoadingViewHolder extends GenericViewHolder {

    public LoadingViewHolder(View itemView) {
        super(itemView);
    }

    @Override
    public void onBindViewHolder(Object item) {
        super.onBindViewHolder(item);
    }

}