package br.com.rmoreira.app.helpers;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by robsonmoreira on 11/10/17.
 */

public final class DateHelper {

    public static final Locale LOCALE_PT_BR = new Locale("pt", "BR");

    private static final String PATTERN_DATE = "dd/MM/yyyy";
    private static final String PATTERN_TIME = "HH:mm:ss";
    private static final String PATTERN_DATETIME = "dd/MM/yyyy HH:mm:ss";

    public static String formatDate(Date date) {
        return format(date, DateHelper.PATTERN_DATE);
    }

    public static String formatDateTime(Date date) {
        return format(date, DateHelper.PATTERN_DATETIME);
    }

    public static String formatTime(Date date) {
        return format(date, DateHelper.PATTERN_TIME);
    }

    private static String format(Date date, String pattern) {
        final DateFormat dateTimeFormat = new SimpleDateFormat(pattern, DateHelper.LOCALE_PT_BR);
        return dateTimeFormat.format(date);
    }

}
