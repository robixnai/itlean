package br.com.rmoreira.app.adapters.holders;

import android.view.View;

import br.com.rmoreira.app.contracts.OnItemClickListener;

/**
 * Created by robsonmoreira on 10/10/17.
 */

public class NoDataViewHolder extends GenericViewHolder {

    public NoDataViewHolder(View itemView, OnItemClickListener listener) {
        super(itemView, listener);
    }

    @Override
    public void onBindViewHolder(Object item) {
        super.onBindViewHolder(item);
    }

}