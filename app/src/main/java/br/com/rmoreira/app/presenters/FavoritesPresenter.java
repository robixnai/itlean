package br.com.rmoreira.app.presenters;

import java.util.List;

import br.com.rmoreira.app.contracts.AppRss;
import br.com.rmoreira.app.dao.pojo.News;
import br.com.rmoreira.app.models.FavoritesModel;

/**
 * Created by robsonmoreira on 10/10/17.
 */

public class FavoritesPresenter implements AppRss.FaviritesPresenterImpl {

    private AppRss.FaviritesViewImpl mView;
    private FavoritesModel mModel;

    public FavoritesPresenter(AppRss.FaviritesViewImpl faviritesView) {
        mView = faviritesView;
        mModel = new FavoritesModel(this);
    }

    @Override
    public void getNewsList() {
        mModel.getNewsList();
    }

    @Override
    public void updateNewsList(List<News> newsList) {
        if (newsList == null) {
            mView.noData();
        } else {
            mView.updateNewsList(newsList);
        }
    }

    @Override
    public void remove(News news) {
        mModel.remove(news);
    }
}
