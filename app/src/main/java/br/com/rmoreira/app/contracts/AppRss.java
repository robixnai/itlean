package br.com.rmoreira.app.contracts;

import java.util.List;

import br.com.rmoreira.app.dao.pojo.News;

/**
 * Created by robsonmoreira on 10/10/17.
 */

public interface AppRss {

    interface HomeModelImpl {
        void getNewsList();
        boolean isSaved(News news);
        void save(News news);
        void remove(News news);
    }
    interface HomeViewImpl {
        void getNewsList();
        void updateNewsList(List<News> newsList, boolean loading);
        boolean isSaved(News news);
        void save(News news);
        void remove(News news);
        void showProgressBar(int visibility);
        void showMissingConnection();
    }
    interface HomePresenterImpl {
        void getNewsList(int page);
        void updateNewsList(List<News> newsList);
        boolean isSaved(News news);
        void save(News news);
        void remove(News news);
    }

    interface FaviritesModelImpl {
        void getNewsList();
        void remove(News news);
    }
    interface FaviritesViewImpl {
        void getNewsList();
        void updateNewsList(List<News> newsList);
        void remove(News news);
        void noData();
    }
    interface FaviritesPresenterImpl {
        void getNewsList();
        void updateNewsList(List<News> newsList);
        void remove(News news);
    }

}
