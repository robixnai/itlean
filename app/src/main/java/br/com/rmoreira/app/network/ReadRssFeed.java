package br.com.rmoreira.app.network;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import br.com.rmoreira.app.dao.pojo.News;

/**
 * Created by robsonmoreira on 10/10/17.
 */

public class ReadRssFeed {

    private boolean mIsItem;
    private News mNews;
    private List<News> mNewsList;

    public ReadRssFeed() {
        mIsItem = false;
        mNews = null;
        mNewsList = null;
    }

    public List<News> read() {
        try {
            URL url = new URL("http://www.androidauthority.com/feed/");

            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(false);
            XmlPullParser xpp = factory.newPullParser();
            xpp.setInput(getInputStream(url), "UTF_8");

            mNewsList = new ArrayList<>();
            int eventType = xpp.getEventType();
            while (eventType != XmlPullParser.END_DOCUMENT) {
                if (eventType == XmlPullParser.START_TAG) {
                    parseItem(xpp, mNewsList);
                }
                eventType = xpp.next();
            }
        } catch (XmlPullParserException | IOException e) {
            e.printStackTrace();
        }
        return mNewsList;
    }

    private InputStream getInputStream(URL url) throws IOException {
        return url.openConnection().getInputStream();
    }

    private void parseItem(XmlPullParser xpp, List<News> newsList) throws XmlPullParserException, IOException {
        if (xpp.getName().equalsIgnoreCase("item")) {
            mIsItem = true;
            mNews = new News();
        } else if (xpp.getName().equalsIgnoreCase("title")) {
            if (mIsItem)
                mNews.setTitle(xpp.nextText());
        } else if (xpp.getName().equalsIgnoreCase("link")) {
            if (mIsItem)
                mNews.setLink(xpp.nextText());
        } else if (xpp.getName().equalsIgnoreCase("comments")) {
            if (mIsItem)
                mNews.setComments(xpp.nextText());
        } else if (xpp.getName().equalsIgnoreCase("pubDate")) {
            if (mIsItem)
                mNews.setPubDate(new Date(xpp.nextText()));
        } else if (xpp.getName().equalsIgnoreCase("dc:creator")) {
            if (mIsItem)
                mNews.setCreator(xpp.nextText());
        } else if (xpp.getName().equalsIgnoreCase("description")) {
            if (mIsItem)
                mNews.setDescription(xpp.nextText());
        } else if (xpp.getName().equalsIgnoreCase("media:content")) {
            if (mIsItem)
                mNews.setImageUrl(xpp.getAttributeValue("", "url"));
        } else if (xpp.getName().equalsIgnoreCase("content:encoded")) {
            if (mIsItem)
                mNews.setContent(xpp.nextText());
        } else if (xpp.getName().equalsIgnoreCase("wfw:commentRss")) {
            if (mIsItem) {
                mNews.setCommentRss(xpp.nextText());
                newsList.add(mNews);
            }
        }
    }

}
