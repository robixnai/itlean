package br.com.rmoreira.app.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;
import java.util.Objects;

import br.com.rmoreira.app.R;
import br.com.rmoreira.app.adapters.holders.GenericViewHolder;
import br.com.rmoreira.app.adapters.holders.NewsViewHolder;
import br.com.rmoreira.app.adapters.holders.LoadingViewHolder;
import br.com.rmoreira.app.adapters.holders.NoDataViewHolder;
import br.com.rmoreira.app.adapters.holders.TryAgainViewHolder;
import br.com.rmoreira.app.contracts.OnItemClickListener;
import br.com.rmoreira.app.dao.pojo.News;

/**
 * Created by robsonmoreira on 10/10/17.
 */

public class GenericAdapter extends RecyclerView.Adapter<GenericViewHolder> {

    private List<Object> mItens;
    private boolean mIsSaved;
    private OnItemClickListener mListener;

    public GenericAdapter(List<Object> newsList, boolean isSaved, OnItemClickListener listener) {
        mItens = newsList;
        mIsSaved = isSaved;
        mListener = listener;
    }

    @Override
    public GenericViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        if (viewType == GenericType.HOME_NEWS.ordinal()) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_news_itens, parent, false);
            return new NewsViewHolder(parent.getContext(), view, mIsSaved, mListener);
        } else if (viewType == GenericType.LOADING.ordinal()) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_loading, parent, false);
            return new LoadingViewHolder(view);
        } else if (viewType == GenericType.TRY_AGAIN.ordinal()) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_try_again, parent, false);
            return new TryAgainViewHolder(view, mListener);
        } else if (viewType == GenericType.NOT_DATA.ordinal()) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_not_data, parent, false);
            return new NoDataViewHolder(view, mListener);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(GenericViewHolder holder, int position) {
        Object item = mItens.get(position);
        holder.onBindViewHolder(item);
    }

    @Override
    public int getItemViewType(int position) {
        Object item = mItens.get(position);
        if (item instanceof News)
            return GenericType.HOME_NEWS.ordinal();
        else if (item.toString().equals(GenericType.LOADING.getmAbbreviation()))
            return GenericType.LOADING.ordinal();
        else if (item.toString().equals(GenericType.TRY_AGAIN.getmAbbreviation()))
            return GenericType.TRY_AGAIN.ordinal();
        else if (item.toString().equals(GenericType.NOT_DATA.getmAbbreviation()))
            return GenericType.NOT_DATA.ordinal();
        else return -1;
    }

    @Override
    public int getItemCount() {
        return mItens != null ? mItens.size() : 0;
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    public void removeItem(Object object) {
        mItens.remove(object);
        notifyDataSetChanged();
        if (mItens.isEmpty()) {
            showNotData();
        }
    }

    public void updateItem(Object object) {
        int index = mItens.indexOf(object);
        mItens.remove(object);
        mItens.add(index, object);
        notifyDataSetChanged();
    }

    public void updateItens(List<Object> itens, boolean loading) {
        mItens = itens;
        if (loading) {
            if (mItens.size() > 0) {
                mItens.add(GenericType.LOADING.getmAbbreviation());
            }
        }
        notifyDataSetChanged();
    }

    public void showMissingConnection() {
        mItens.clear();
        mItens.add(GenericType.TRY_AGAIN.getmAbbreviation());
        notifyDataSetChanged();
    }

    public void showNotData() {
        mItens.clear();
        mItens.add(GenericType.NOT_DATA.getmAbbreviation());
        notifyDataSetChanged();
    }

}
