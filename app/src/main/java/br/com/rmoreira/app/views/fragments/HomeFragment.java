package br.com.rmoreira.app.views.fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.*;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import br.com.rmoreira.app.R;
import br.com.rmoreira.app.adapters.EndlessRecyclerOnScrollListener;
import br.com.rmoreira.app.adapters.GenericAdapter;
import br.com.rmoreira.app.adapters.GenericType;
import br.com.rmoreira.app.contracts.AppRss;
import br.com.rmoreira.app.contracts.OnItemClickListener;
import br.com.rmoreira.app.dao.pojo.News;
import br.com.rmoreira.app.presenters.HomePresenter;
import br.com.rmoreira.app.views.activities.NewsDetailsActivity;

public class HomeFragment extends Fragment implements AppRss.HomeViewImpl, OnItemClickListener {

    private View mView;
    private OnScrollListener mOnScrollListener;
    private RecyclerView mRecyclerView;
    private GenericAdapter mAdapter;
    private LinearLayoutManager mLayoutManager;

    private HomePresenter mHomePresenter;
    private List<Object> mNewsList = new ArrayList<>();

    private int mPage = 1;

    public HomeFragment() {
    }

    public static HomeFragment newInstance() {
        return new HomeFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_home, container, false);

        mRecyclerView = (RecyclerView) mView.findViewById(R.id.recyclerView);
        mRecyclerView.setHasFixedSize(true);

        mLayoutManager = new LinearLayoutManager(mView.getContext());
        mRecyclerView.setLayoutManager(mLayoutManager);

        refreshScrollListener(mRecyclerView);

        mAdapter = new GenericAdapter(mNewsList, false, this);
        mRecyclerView.setAdapter(mAdapter);

        getNewsList();

        return mView;
    }

    @Override
    public void getNewsList() {
        if (mHomePresenter == null)
            mHomePresenter = new HomePresenter(this);
        mHomePresenter.getNewsList(mPage);
    }

    @Override
    public void updateNewsList(List<News> newsList, boolean loading) {
        if (mNewsList != null && mNewsList.size() > 0) {
            mNewsList.remove(mNewsList.size() - 1);
        }
        if (newsList != null && mNewsList != null) {
            mNewsList.addAll(newsList);
        }
        mAdapter.updateItens(mNewsList, loading);
    }

    @Override
    public boolean isSaved(News news) {
        return mHomePresenter.isSaved(news);
    }

    @Override
    public void save(News news) {
        mHomePresenter.save(news);
    }

    @Override
    public void remove(News news) {
        mHomePresenter.remove(news);
    }

    @Override
    public void showProgressBar(int visibility) {
        mView.findViewById(R.id.includeProgressBar).setVisibility(visibility);
    }

    @Override
    public void showMissingConnection() {
        mAdapter.showMissingConnection();
    }

    @Override
    public void onItemClick(View view, Object object) {
        if (object instanceof News) {
            switch (view.getId()) {
                case R.id.imageViewSave:
                    boolean isSaved = isSaved((News) object);
                    if (!isSaved) {
                        save((News) object);
                    } else {
                        remove((News) object);
                    }
                    ((News) object).setSaved(!isSaved);
                    mAdapter.updateItem(object);
                    break;
                case R.id.imageViewShare:
                    Intent share = new Intent(Intent.ACTION_SEND);
                    share.setType("text/plain");
                    share.putExtra(Intent.EXTRA_TEXT, ((News) object).getLink());
                    startActivity(Intent.createChooser(share, "Selecione uma da opções"));
                    break;
                case R.id.imageViewBrowser:
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(((News) object).getLink()));
                    startActivity(browserIntent);
                    break;
                default:
                    Intent intent = new Intent(getActivity(), NewsDetailsActivity.class);
                    intent.putExtra(NewsDetailsActivity.NEWS, (News) object);
                    startActivity(intent);
                    break;
            }
        } else if (object.toString().equals(GenericType.TRY_AGAIN.getmAbbreviation())) {
            mNewsList = new ArrayList<>();
            getNewsList();
        }
    }

    private void refreshScrollListener(RecyclerView recyclerView) {
        if (mOnScrollListener != null) {
            recyclerView.removeOnScrollListener(mOnScrollListener);
        }
        mOnScrollListener = new EndlessRecyclerOnScrollListener(mLayoutManager) {
            @Override
            public void onLoadMore(int current_page) {
                mPage = current_page;
                getNewsList();
            }
        };
        recyclerView.addOnScrollListener(mOnScrollListener);
    }

}
