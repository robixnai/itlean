package br.com.rmoreira.app.presenters;

import android.view.View;

import java.util.ArrayList;
import java.util.List;

import br.com.rmoreira.app.AppRssApplication;
import br.com.rmoreira.app.contracts.AppRss;
import br.com.rmoreira.app.models.HomeModel;
import br.com.rmoreira.app.dao.pojo.News;

/**
 * Created by robsonmoreira on 10/10/17.
 */

public class HomePresenter implements AppRss.HomePresenterImpl {

    private AppRss.HomeViewImpl mView;
    private HomeModel mModel;

    public HomePresenter(AppRss.HomeViewImpl homeView) {
        mView = homeView;
        mModel = new HomeModel(this);
    }

    @Override
    public void getNewsList(int page) {
        if (AppRssApplication.isNetworkAvailable()) {
            if (page == 1) {
                mView.showProgressBar(View.VISIBLE);
            }
            mModel.getNewsList();
        } else {
            mView.showMissingConnection();
        }
    }

    @Override
    public void updateNewsList(List<News> newsList) {
        List<News> newses = null;
        if (newsList != null && !newsList.isEmpty()) {
            newses = new ArrayList<>();
            for (News news : newsList) {
                news.setSaved(isSaved(news));
                newses.add(news);
            }
        }
        mView.updateNewsList(newses, newses != null);
        mView.showProgressBar(View.GONE);
    }

    @Override
    public boolean isSaved(News news) {
        return mModel.isSaved(news);
    }

    @Override
    public void save(News news) {
        mModel.save(news);
    }

    @Override
    public void remove(News news) {
        mModel.remove(news);
    }

}
