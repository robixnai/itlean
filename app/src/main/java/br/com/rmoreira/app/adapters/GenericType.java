package br.com.rmoreira.app.adapters;

/**
 * Created by robsonmoreira on 10/10/17
 */

public enum GenericType {

    LOADING("L"),
    HOME_NEWS("N"),
    TRY_AGAIN("T"),
    NOT_DATA("D");

    private String mAbbreviation;

    GenericType(String abbreviation) {
        mAbbreviation = abbreviation;
    }

    public String getmAbbreviation() {
        return mAbbreviation;
    }

}