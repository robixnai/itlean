package br.com.rmoreira.app.models;

import android.os.AsyncTask;

import java.util.List;

import br.com.rmoreira.app.contracts.AppRss;
import br.com.rmoreira.app.dao.NewsDao;
import br.com.rmoreira.app.dao.entities.NewsEntity;
import br.com.rmoreira.app.network.ReadRssFeed;
import br.com.rmoreira.app.dao.pojo.News;

/**
 * Created by robsonmoreira on 10/10/17.
 */

public class HomeModel implements AppRss.HomeModelImpl {

    private AppRss.HomePresenterImpl mPresenter;

    public HomeModel(AppRss.HomePresenterImpl homePresenter) {
        mPresenter = homePresenter;
    }

    @Override
    public void getNewsList() {
        new AsyncTask<Void, Void, List<News>>() {

            @Override
            protected List<News> doInBackground(Void... voids) {
                return new ReadRssFeed().read();
            }

            @Override
            protected void onPostExecute(List<News> newsList) {
                super.onPostExecute(newsList);
                mPresenter.updateNewsList(newsList);
            }

        }.execute();
    }

    @Override
    public boolean isSaved(News news) {
        NewsEntity newsEntity = new NewsEntity(news.getTitle(), news.getLink(), news.getComments(), news.getPubDate(),
                news.getCreator(), news.getDescription(), news.getContent(), news.getImageUrl(), news.getCommentRss());
        return NewsDao.getInstance().getModel(newsEntity) != null;
    }

    @Override
    public void save(News news) {
        NewsEntity newsEntity = new NewsEntity(news.getTitle(), news.getLink(), news.getComments(), news.getPubDate(),
                news.getCreator(), news.getDescription(), news.getContent(), news.getImageUrl(), news.getCommentRss());
        NewsDao.getInstance().saveModel(newsEntity);
    }

    @Override
    public void remove(News news) {
        NewsEntity newsEntity = new NewsEntity(news.getTitle(), news.getLink(), news.getComments(), news.getPubDate(),
                news.getCreator(), news.getDescription(), news.getContent(), news.getImageUrl(), news.getCommentRss());
        NewsDao.getInstance().removeModel(newsEntity);
    }

}
