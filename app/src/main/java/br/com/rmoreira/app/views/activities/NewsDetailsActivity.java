package br.com.rmoreira.app.views.activities;

import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import br.com.rmoreira.app.R;
import br.com.rmoreira.app.dao.pojo.News;

public class NewsDetailsActivity extends AppCompatActivity {

    public static final String NEWS = "news";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_details);

        News news = getIntent().getExtras().getParcelable(NEWS);

        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        CollapsingToolbarLayout collapsingToolbar =
                (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        collapsingToolbar.setTitle(news.getTitle());

        ImageView imageView = (ImageView) findViewById(R.id.backdrop);
        Glide.with(this).load(news.getImageUrl()).into(imageView);

        TextView content = (TextView) findViewById(R.id.textViewContent);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            content.setText(Html.fromHtml(news.getContent(), Html.FROM_HTML_MODE_COMPACT));
        } else {
            content.setText(Html.fromHtml(news.getContent()));
        }
    }

}
