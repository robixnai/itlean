package br.com.rmoreira.app.views.fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import br.com.rmoreira.app.R;
import br.com.rmoreira.app.adapters.GenericAdapter;
import br.com.rmoreira.app.contracts.AppRss;
import br.com.rmoreira.app.contracts.OnItemClickListener;
import br.com.rmoreira.app.dao.pojo.News;
import br.com.rmoreira.app.presenters.FavoritesPresenter;
import br.com.rmoreira.app.views.activities.NewsDetailsActivity;

public class FavoritesFragment extends Fragment implements AppRss.FaviritesViewImpl, OnItemClickListener {

    private View mView;
    private RecyclerView mRecyclerView;
    private GenericAdapter mAdapter;
    private LinearLayoutManager mLayoutManager;

    private FavoritesPresenter mFavoritesPresenter;
    private List<Object> mNewsList = new ArrayList<>();

    public FavoritesFragment() {
    }

    public static FavoritesFragment newInstance() {
        return new FavoritesFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_favorites, container, false);

        mRecyclerView = (RecyclerView) mView.findViewById(R.id.recyclerView);
        mRecyclerView.setHasFixedSize(true);

        mLayoutManager = new LinearLayoutManager(mView.getContext());
        mRecyclerView.setLayoutManager(mLayoutManager);

        mAdapter = new GenericAdapter(mNewsList, true, this);
        mRecyclerView.setAdapter(mAdapter);

        getNewsList();

        return mView;
    }

    @Override
    public void getNewsList() {
        if (mFavoritesPresenter == null)
            mFavoritesPresenter = new FavoritesPresenter(this);
        mFavoritesPresenter.getNewsList();
    }

    @Override
    public void updateNewsList(List<News> newsList) {
        if (mNewsList != null && mNewsList.size() > 0) {
            mNewsList.remove(mNewsList.size() - 1);
        }
        if (newsList != null && mNewsList != null) {
            mNewsList.addAll(newsList);
        }
        mAdapter.updateItens(mNewsList, false);
    }

    @Override
    public void remove(News news) {
        mFavoritesPresenter.remove(news);
    }

    @Override
    public void noData() {
        mAdapter.showNotData();
    }

    @Override
    public void onItemClick(View view, Object object) {
        if (object instanceof News) {
            switch (view.getId()) {
                case R.id.imageViewSave:
                    remove((News) object);
                    mAdapter.removeItem(object);
                    break;
                case R.id.imageViewShare:
                    Intent share = new Intent(Intent.ACTION_SEND);
                    share.setType("text/plain");
                    share.putExtra(Intent.EXTRA_TEXT, ((News) object).getLink());
                    startActivity(Intent.createChooser(share, "Selecione uma da opções"));
                    break;
                case R.id.imageViewBrowser:
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(((News) object).getLink()));
                    startActivity(browserIntent);
                    break;
                default:
                    Intent intent = new Intent(getActivity(), NewsDetailsActivity.class);
                    intent.putExtra(NewsDetailsActivity.NEWS, (News) object);
                    startActivity(intent);
                    break;
            }
        }
    }

}
