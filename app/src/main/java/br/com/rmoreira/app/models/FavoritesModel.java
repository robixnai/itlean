package br.com.rmoreira.app.models;

import com.activeandroid.Model;

import java.util.ArrayList;
import java.util.List;

import br.com.rmoreira.app.contracts.AppRss;
import br.com.rmoreira.app.dao.NewsDao;
import br.com.rmoreira.app.dao.entities.NewsEntity;
import br.com.rmoreira.app.dao.pojo.News;

/**
 * Created by robsonmoreira on 10/10/17.
 */

public class FavoritesModel implements AppRss.FaviritesModelImpl {

    private AppRss.FaviritesPresenterImpl mPresenter;

    public FavoritesModel(AppRss.FaviritesPresenterImpl faviritesPresenter) {
        mPresenter = faviritesPresenter;
    }

    @Override
    public void getNewsList() {
        List<News> newsList = null;
        List<Model> models = NewsDao.getInstance().getModelList();
        if (models != null && !models.isEmpty()) {
            newsList = new ArrayList<>();
            for (Model model : models) {
                NewsEntity newsEntity = (NewsEntity) model;
                News news = new News(newsEntity.getTitle(), newsEntity.getLink(), newsEntity.getComments(), newsEntity.getPubDate(), newsEntity.getCreator(),
                        newsEntity.getDescription(), newsEntity.getContent(), newsEntity.getImageUrl(), newsEntity.getCommentRss());
                newsList.add(news);
            }
        }
        mPresenter.updateNewsList(newsList);
    }

    @Override
    public void remove(News news) {
        NewsEntity newsEntity = new NewsEntity(news.getTitle(), news.getLink(), news.getComments(), news.getPubDate(),
                news.getCreator(), news.getDescription(), news.getContent(), news.getImageUrl(), news.getCommentRss());
        NewsDao.getInstance().removeModel(newsEntity);
    }
}
