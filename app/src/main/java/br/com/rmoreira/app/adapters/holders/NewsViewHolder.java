package br.com.rmoreira.app.adapters.holders;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import br.com.rmoreira.app.R;
import br.com.rmoreira.app.contracts.OnItemClickListener;
import br.com.rmoreira.app.dao.pojo.News;
import br.com.rmoreira.app.helpers.DateHelper;

/**
 * Created by robsonmoreira on 10/10/17.
 */

public final class NewsViewHolder extends GenericViewHolder<News> {

    private ImageView mImageView, mImageViewSave, mImageViewShare, mImageViewBrowser;
    private TextView mTextViewTitle, mTextViewCreator, mTextViewPubDate, mTextViewDescription;

    private Context mContext;
    private boolean mIsSaved;

    public NewsViewHolder(Context context, View view, boolean isSaved, OnItemClickListener listener) {
        super(view, listener);

        mContext = context;
        mIsSaved = isSaved;

        mImageView = (ImageView) view.findViewById(R.id.imageView);
        mImageViewSave = (ImageView) view.findViewById(R.id.imageViewSave);
        mImageViewShare = (ImageView) view.findViewById(R.id.imageViewShare);
        mImageViewBrowser = (ImageView) view.findViewById(R.id.imageViewBrowser);

        mTextViewTitle = (TextView) view.findViewById(R.id.textViewTitle);
        mTextViewCreator = (TextView) view.findViewById(R.id.textViewCreator);
        mTextViewPubDate = (TextView) view.findViewById(R.id.textViewPubDate);
        mTextViewDescription = (TextView) view.findViewById(R.id.textViewDescription);

        mImageViewSave.setOnClickListener(this);
        mImageViewShare.setOnClickListener(this);
        mImageViewBrowser.setOnClickListener(this);

        if (mIsSaved) {
            mImageViewSave.setImageResource(R.drawable.ic_delete);
        }
    }

    @Override
    public void onBindViewHolder(News news) {
        super.onBindViewHolder(news);

        Glide.with(mContext).load(news.getImageUrl()).into(mImageView);

        mTextViewTitle.setText(news.getTitle());
        mTextViewCreator.setText(news.getCreator());
        mTextViewPubDate.setText(DateHelper.formatDateTime(news.getPubDate()));
        mTextViewDescription.setText(news.getDescription());

        if (!mIsSaved) {
            if (news.isSaved()) {
                mImageViewSave.setImageResource(R.drawable.ic_saved);
            } else {
                mImageViewSave.setImageResource(R.drawable.ic_save);
            }
        }
    }

}
