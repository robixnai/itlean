package br.com.rmoreira.app.dao.entities;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

import java.util.Date;

/**
 * Created by robsonmoreira on 10/10/17.
 */

@Table(name = NewsEntity.TABLE_NAME)
public class NewsEntity extends Model {

    public static final String TABLE_NAME = "News";
    public static final String TITLE = "Title";
    public static final String LINK = "Link";
    public static final String COMMENTS = "Comments";
    public static final String PUB_DATE = "PubDate";
    public static final String CREATOR = "Creator";
    public static final String DESCRIPTION = "Description";
    public static final String CONTENT = "Content";
    public static final String IMAGE_URL = "ImageUrl";
    public static final String COMMENT_RSS = "CommentRss";

    @Column(name = TITLE)
    private String title;

    @Column(name = LINK)
    private String link;

    @Column(name = COMMENTS)
    private String comments;

    @Column(name = PUB_DATE)
    private Date pubDate;

    @Column(name = CREATOR)
    private String creator;

    @Column(name = DESCRIPTION)
    private String description;

    @Column(name = CONTENT)
    private String content;

    @Column(name = IMAGE_URL)
    private String imageUrl;

    @Column(name = COMMENT_RSS)
    private String commentRss;

    public NewsEntity() {
        super();
    }

    public NewsEntity(String title, String link, String comments, Date pubDate, String creator, String description,
                      String content, String imageUrl, String commentRss) {
        this.title = title;
        this.link = link;
        this.comments = comments;
        this.pubDate = pubDate;
        this.creator = creator;
        this.description = description;
        this.content = content;
        this.imageUrl = imageUrl;
        this.commentRss = commentRss;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public Date getPubDate() {
        return pubDate;
    }

    public void setPubDate(Date pubDate) {
        this.pubDate = pubDate;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getCommentRss() {
        return commentRss;
    }

    public void setCommentRss(String commentRss) {
        this.commentRss = commentRss;
    }

}
