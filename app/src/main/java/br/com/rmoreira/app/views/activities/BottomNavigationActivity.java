package br.com.rmoreira.app.views.activities;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import br.com.rmoreira.app.R;
import br.com.rmoreira.app.views.fragments.FavoritesFragment;
import br.com.rmoreira.app.views.fragments.HomeFragment;

public class BottomNavigationActivity extends AppCompatActivity
        implements BottomNavigationView.OnNavigationItemSelectedListener {

    private BottomNavigationView mBottomNavigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bottom_navigation);

        mBottomNavigationView = (BottomNavigationView) findViewById(R.id.navigation);
        mBottomNavigationView.setOnNavigationItemSelectedListener(this);

        setFragmentTransaction(HomeFragment.newInstance());
        setTitle(R.string.title_home);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.navigation_home:
                setFragmentTransaction(HomeFragment.newInstance());
                setTitle(R.string.title_home);
                return true;
            case R.id.navigation_dashboard:
                setFragmentTransaction(FavoritesFragment.newInstance());
                setTitle(R.string.title_saved);
                return true;
        }
        return false;
    }

    @NonNull
    private void setFragmentTransaction(Fragment fragment) {
        final FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.content, fragment).commitAllowingStateLoss();
    }

}
