package br.com.rmoreira.app.dao;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.Model;
import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;

import java.util.List;

import br.com.rmoreira.app.dao.entities.NewsEntity;

/**
 * Created by robsonmoreira on 10/10/17.
 */

public class NewsDao {

    private static NewsDao INSTANCE;

    private NewsDao() {}

    public static synchronized NewsDao getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new NewsDao();
        }
        return INSTANCE;
    }

    public List<Model> getModelList() {
        List<Model> modelList = new Select().all().from(NewsEntity.class).execute();
        return modelList;
    }

    public Model getModel(NewsEntity newsEntity) {
        Model model = new Select().from(NewsEntity.class).where("Title = ?", newsEntity.getTitle()).executeSingle();
        return model;
    }

    public void saveModel(Model model) {
        ActiveAndroid.beginTransaction();
        try {
            model.save();
            ActiveAndroid.setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            ActiveAndroid.endTransaction();
        }
    }

    public void removeModel(NewsEntity newsEntity) {
        new Delete().from(NewsEntity.class).where("Title = ?", newsEntity.getTitle()).execute();
    }

}
